#! /bin/bash

# fetch newest image
docker pull registry.gitlab.com/fdroid/fdroid-website-search:production

# try nuke previously started container
docker stop fdroid-website-search
docker rm fdroid-website-search

# start new container
docker run --detach --restart=always --name fdroid-website-search --publish 127.0.0.1:8000:8000 --mount type=tmpfs,destination=/tmpfs_fws,tmpfs-size=1073741824 registry.gitlab.com/fdroid/fdroid-website-search:production

# TODO: remove this once the old staging setup is decommisioned and and the workaround removed
docker exec fdroid-website-search sed -i "s/<link href=\"\/search{/<link href=\"{/" fdroid_website_search/templates/search/search.html

# nuke outdated docker images
OLD=`docker images | grep -v production | grep -v REPOSITORY | awk '{print $3}'`
for O in $OLD; do
    docker rmi $O
done
